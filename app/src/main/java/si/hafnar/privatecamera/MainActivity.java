package si.hafnar.privatecamera;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.veinhorn.scrollgalleryview.ScrollGalleryView;

import org.apache.commons.io.comparator.LastModifiedFileComparator;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ScrollGalleryView scrollGalleryView;
    final private int PICK_IMAGE = 1;
    final private int CAPTURE_IMAGE = 2;
    private String imgPath;
    String path;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Set the path for saving pictures
        path = (Environment.getExternalStorageDirectory() + "/PrivateCamera/");

        // Set the Gallery parameters
        scrollGalleryView = (ScrollGalleryView) findViewById(R.id.scroll_gallery_view);
        scrollGalleryView
                .setThumbnailSize(100)
                .setZoom(true)
                .setFragmentManager(getSupportFragmentManager());

        // Call the method that loads the pictures
        loadPictures();

        // Open camera on click
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();
            }
        });
    }

    // Open the external camera
    public void openCamera() {
        final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
        startActivityForResult(intent, CAPTURE_IMAGE);
    }

    // Only load the files with the agreed extension
    static final FilenameFilter imageFilter = new FilenameFilter() {
        @Override
        public boolean accept(final File dir, final String name) {
            if (name.endsWith(".hef")) {
                return (true);
            }
            return (false);
        }
    };

    // Called from camera, saves the picture
    public Uri setImageUri() {
        // Create folder if it doesn't exist
        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdir();
        }
        // Store image in in the default folder with default extension
        File file = new File(path, new Date().getTime() + ".hef");
        Uri imgUri = Uri.fromFile(file);
        this.imgPath = file.getAbsolutePath();
        return imgUri;
    }

    /*
    public String getImagePath() {
        return imgPath;
    }
    */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO check if of any use at all
        if ((resultCode != Activity.RESULT_CANCELED) && (resultCode == RESULT_OK && data != null)) {
            if (requestCode == PICK_IMAGE) {
                //   selectedImagePath = getAbsolutePath(data.getData());
                //   Bitmap b = createImage(selectedImagePath);
                //   scrollGalleryView.addImage(b);
            } else if (requestCode == CAPTURE_IMAGE) {
                //       selectedImagePath = getAbsolutePath(data.getData());
                //       Bitmap b = createImage(selectedImagePath);
                //       scrollGalleryView.addImage(b);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

        // Restarts the activity after the picture is taken to include new pics
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);

    }

    // Loads the Gallery
    public void loadPictures() {
        File dir = new File(path);
        File[] filelist = dir.listFiles(imageFilter);

        // Load pictures if the folder exists and is not empty
        if ((filelist != null) && (filelist.length > 0)) {

            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = false;
            options.inSampleSize = 4;
            options.inTempStorage = new byte[16 * 1024];

            List listFile = Arrays.asList(dir.list());
            Collections.sort(listFile, Collections.reverseOrder());

            Arrays.sort(filelist, LastModifiedFileComparator.LASTMODIFIED_REVERSE);

            for (File f : filelist) {
                Bitmap bitmap = BitmapFactory.decodeFile(f.getPath(), options);
                scrollGalleryView.addImage(bitmap);
            }

        } else {
            // If there is no folder or is empty, open the external camera
            openCamera();
        }
    }
/*
    public Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 70;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;

    }

    public String getAbsolutePath(Uri uri) {
        String[] projection = {MediaStore.MediaColumns.DATA};
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }
    */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // Placeholder text for the settings
        if (id == R.id.action_settings) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "PIN for the App, encryption and similar options will be available here.", Snackbar.LENGTH_LONG);
            snackbar.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    /*
    private Bitmap createImage(String picName) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inSampleSize = 4;
        options.inTempStorage = new byte[16 * 1024];
        Bitmap bitmap = BitmapFactory.decodeFile(picName, options);
        return bitmap;
    }
    */

    /*
    public void getPermission(){
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Explain to the user why we need to read the contacts
            }

            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant

            return;
        }

    }
    */
}